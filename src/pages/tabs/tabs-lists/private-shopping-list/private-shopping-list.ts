import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ModalController, AlertController } from 'ionic-angular';
import { FlashProvider } from '../../../../providers/shared/flash/flash';
import { ShoppingListProvider } from '../../../../providers/shopping-list/shopping-list';
import { Toast } from '@ionic-native/toast';
import { LocalStorageProvider } from '../../../../providers/local-storage/local-storage';

@IonicPage()
@Component({
  selector: 'page-private-shopping-list',
  templateUrl: 'private-shopping-list.html',
})
export class PrivateShoppingListPage {

  loading: any;

  lists: any[] = [];
  page = 1;
  perPage = 0;
  total = 0;
  totalPage = 0;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public toast: Toast,
    public flashProvider: FlashProvider,
    public localStorageProvider: LocalStorageProvider,
    public _shoppingListProvider: ShoppingListProvider
  ) {
  }

  ionViewDidEnter() {
    this.getLists(true);
  }

  async getLists(clearList = false, page?: number, wBy?: string, w?: string) {
    this.showLoader();

    // Id do tipo Categoria no BD é 1
    await this._shoppingListProvider.getAll(true, true, page, wBy, w)
      .then((res) => {
        this.total = res['data']['total'];
        this.perPage = res['data']['perPage'];
        this.page = res['data']['page'];

        if (clearList) {
          this.lists = [];
        }

        res['data']['data'].forEach(element => {
          this.lists.push(element)
        });

        this.localStorageProvider.set('lists', this.lists);
        this.loading.dismiss();
      }, (err) => {
        console.log('erro ', err);
        
        this.loading.dismiss();
        if (err.status !== 404) {
          this.flashProvider.show('Não foi possível buscar as listas no servidor!', 'error', 3000)
        }

        this.localStorageProvider.get('lists').then(res => {
          this.lists = res ? res : [];
        });
      });
  }

  newList() {
    const modal = this.modalCtrl.create('FormShoppingListPage');
    modal.present();

    modal.onDidDismiss(data => {
      if(data && data.code) {
        this.getLists(true);
      }
    });
  }

  showToast(message) {
    this.toast.show(message, '5000', 'bottom').subscribe(toast => { });
  }

  goDetails(params) {
    this.navCtrl.push('DetailsShoppingListPage', {data: params})
  }

  showLoader() {
    this.loading = this.loadingCtrl.create({
      content: 'Aguarde...'
    });

    this.loading.present();
  }

  doInfinite(infiniteScroll) {
    this.page += 1;

    setTimeout(() => {
      this.getLists(false, this.page);
      infiniteScroll.complete();
    }, 2000);
  }

  async doRefresh(refresher) {
    setTimeout(async () => {
      await this.getLists(true);
      refresher.complete();
    }, 2000);
  }
}
