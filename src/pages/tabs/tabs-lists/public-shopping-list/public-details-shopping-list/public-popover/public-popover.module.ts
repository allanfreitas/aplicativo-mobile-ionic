import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PublicPopoverPage } from './public-popover';

@NgModule({
  declarations: [
    PublicPopoverPage,
  ],
  imports: [
    IonicPageModule.forChild(PublicPopoverPage),
  ]
})
export class PublicPopoverPageModule {}
