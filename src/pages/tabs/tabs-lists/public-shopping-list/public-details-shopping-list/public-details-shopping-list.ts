import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, PopoverController } from 'ionic-angular';

import { FlashProvider } from '../../../../../providers/shared/flash/flash';
import { ShoppingListProductsProvider } from '../../../../../providers/shopping-list/products-shopping-list';
import { ShoppingListProvider } from '../../../../../providers/shopping-list/shopping-list';

@IonicPage()
@Component({
  selector: 'page-public-details-shopping-list',
  templateUrl: 'public-details-shopping-list.html',
})
export class PublicDetailsShoppingListPage {

  loading: any;

  list: any;
  products: any[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public popoverCtrl: PopoverController,
    public flashProvider: FlashProvider,
    public _shoppingListProvider: ShoppingListProvider,
    public _shoppingListProductsProvider: ShoppingListProductsProvider
  ) {
    this.list = this.navParams.get('data');
  }

  async ionViewDidEnter() {
    this.getProductsList();
  }

  getProductsList() {
    this.showLoader();

    this._shoppingListProductsProvider.getAllPublic(this.list['id'])
      .then((res) => {
        this.loading.dismiss();
        this.products = this.imagesProducts(res['data']);
      }, (err) => {
        console.log('erro ', err);

        this.loading.dismiss();
        if (err.status !== 404) {
          this.flashProvider.show('Não foi possível buscar os produtos da lista de compra no servidor.', 'error', 3000)
        }

        // carrega do cache
        this.products = this.imagesProducts(this.list['listProducts']);
      });
  }

  private imagesProducts(products) {
    products.forEach((element, index) => {
      let images = element['product']['images'];
      images.forEach((element, index) => {
        images[index]['url'] = `http://54.233.242.37/image/product/${element['filename']}?format=jpg&quality=80&width=100`
      });
      products[index]['product']['images'] = images;
    });
    return products;
  }

  presentPopover(ev) {
    let popover = this.popoverCtrl.create('PublicPopoverPage', { list: this.list });
    popover.present({ ev: ev });

    // Ação que executa após o popover fechar
    popover.onDidDismiss((res) => {
      if (res && res.status == 'copied') {
        this.navCtrl.pop();
      }
    });
  }

  showLoader() {
    this.loading = this.loadingCtrl.create({
      content: 'Aguarde...'
    });

    this.loading.present();
  }

  async doRefresh(refresher) {
    setTimeout(async () => {
      await this.getProductsList();
      refresher.complete();
    }, 2000);
  }
}
