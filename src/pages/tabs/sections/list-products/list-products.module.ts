import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { ListProductsPage } from './list-products';
import { CardOffersModule } from '../../../../components/card-offers/card-offers.module';

@NgModule({
  declarations: [
    ListProductsPage,
  ],
  imports: [
    IonicPageModule.forChild(ListProductsPage),
    CardOffersModule
  ],
})
export class ListProductsPageModule {}
