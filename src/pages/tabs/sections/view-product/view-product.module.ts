import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { ViewProductPage } from './view-product';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [
    ViewProductPage,
  ],
  imports: [
    IonicPageModule.forChild(ViewProductPage),
    IonicImageLoader
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class ViewProductPageModule {}
