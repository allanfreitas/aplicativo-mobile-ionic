import { Component } from '@angular/core';
import { NavController, IonicPage, LoadingController } from 'ionic-angular';
import lodash from 'lodash';

import { OffersProvider } from '../../../providers/offers/offers';
import { FlashProvider } from '../../../providers/shared/flash/flash';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage  {
  loading: any;

  protected slideShow: any[];
  // protected slidesFreemode: any[];
  // @ViewChild('sliderFreemode') slider: Slides;

  protected offers: { top: Array<Object>, others: Array<Object>, all: Array<Object> }

  constructor(
    public navCtrl: NavController,
    public _offersProvider: OffersProvider,
    public flashProvider: FlashProvider,
    public loadingCtrl: LoadingController
  ) {
    this.offers = {
      others: [],
      top: [],
      all: []
    }

    /*this.slidesFreemode = [
      {
        title: 'Dream\'s Adventure',
        imageUrl: 'assets/img/lists/wishlist-1.jpg',
        songs: 2,
        private: false
      },
      {
        title: 'For the Weekend',
        imageUrl: 'assets/img/lists/wishlist-2.jpg',
        songs: 4,
        private: false
      },
      {
        title: 'Family Time',
        imageUrl: 'assets/img/lists/wishlist-3.jpg',
        songs: 5,
        private: true
      },
      {
        title: 'My Trip',
        imageUrl: 'assets/img/lists/wishlist-4.jpg',
        songs: 12,
        private: true
      },
      {
        title: 'My Trip',
        imageUrl: 'assets/img/lists/wishlist-4.jpg',
        songs: 12,
        private: true
      },
      {
        title: 'My Trip',
        imageUrl: 'assets/img/lists/wishlist-4.jpg',
        songs: 12,
        private: true
      }
    ];*/

    this.slideShow = [
      { image: 'assets/imgs/slide/b1.jpg' },
      { image: 'assets/imgs/slide/b2.jpg' },
      { image: 'assets/imgs/slide/b3.jpg' },
    ];
  }

  ngAfterViewInit() {
    // this.slider.freeMode = true;
  }

  ionViewDidLoad() {
    this.getOffersProductsProgress();
  }

  async doRefresh(refresher) {
    setTimeout(async () => {
      await this.getOffersProductsProgress();
      refresher.complete();
    }, 2000);
  }

  async getOffersProductsProgress() {
    this.showLoader();

    await this._offersProvider.getOffersProductsProgress()
      .then((res) => {
        res['data'].forEach(async (e, i) => {
          if (!!e.type_discount_money && e.discount) {
            res['data'][i]['price_discount'] = e.discount ? e.product_price - e.discount : null;
            res['data'][i]['price_discount_price_two'] = e.discount_price_two && e.discount ? e.product_price - (e.discount + e.discount_price_two) : null;
            res['data'][i]['price_cumulative_per_product'] = e.cumulative && e.discount ? (e.product_price - e.discount) / e.cumulative : null;
            res['data'][i]['percent_discount_per_product'] = await this.calculatePercentualDiscountPerProduct(e);
          } else {
            res['data'][i]['price_discount'] = e.discount ? e.product_price - (e.product_price * e.discount) : null;
            res['data'][i]['price_discount_price_two'] = e.discount_price_two && e.discount ? e.product_price - (e.product_price * (e.discount + e.discount_price_two)) : null;
            res['data'][i]['price_cumulative_per_product'] = e.cumulative ? e.product_price - (e.product_price * (e.discount / e.cumulative)) : null;
            res['data'][i]['percent_discount_per_product'] = await this.calculatePercentualDiscountPerProduct(e);
          }
        });

        const topId = lodash.filter(res['data'], { top_offer: 1 });
        const others = lodash.difference(res['data'], topId);

        this.offers.top = topId;
        this.offers.others = others;
        this.offers.all = res['data'];
        
        this.loading.dismiss();
      }, (err) => {
        this.loading.dismiss();
        if (err.status !== 404) {
          this.flashProvider.show('Erro ao buscar os produtos em oferta', 'error', 3000)
        }
        console.log('erro ', err);
      });
  }

  calculatePercentualDiscountPerProduct(product): number {
    let discount_percent_per_product = 0;

    const price = product.product_price;
    const discount = product.discount;
    const discount_price_two = product.discount_price_two;
    const cumulative = product.cumulative;
    const cumulative_payable = product.cumulative_payable;
    const type_discount_money = product.type_discount_money;
    const type_discount_percentage = product.type_discount_percentage;

    if (type_discount_money && discount) {
      if (discount_price_two && cumulative) { // desconto + desconto preço 2 + acumulativo
        discount_percent_per_product = (discount + discount_price_two) / (price * cumulative);
      } else if (discount_price_two) { // desconto + desconto preço 2
        discount_percent_per_product = (discount + discount_price_two) / price;
      } else if (cumulative) { // desconto + acumulativo
        discount_percent_per_product = discount / (price * cumulative);
      } else { // desconto
        discount_percent_per_product = discount / price;
      }
    } else if (type_discount_percentage && discount) {
      if (discount_price_two && cumulative) { // desconto + desconto preço 2 + acumulativo
        discount_percent_per_product = (discount + discount_price_two) / cumulative;
      } else if (discount_price_two) { // desconto + desconto preço 2
        discount_percent_per_product = discount + discount_price_two;
      } else if (cumulative) { // desconto + acumulativo
        discount_percent_per_product = discount / cumulative;
      } else { // desconto
        discount_percent_per_product = discount;
      }
    } else {
      if (cumulative && cumulative_payable) { // acumulativo + acumulativo a pagar
        discount_percent_per_product = (((cumulative - cumulative_payable) * price) / (price * cumulative)) / cumulative;
      } else if (cumulative) { // acumulativo apenas
        discount_percent_per_product = 0;
      }
    }

    return discount_percent_per_product;
  }

  showLoader() {
    this.loading = this.loadingCtrl.create({
      content: 'Aguarde...'
    });

    this.loading.present();
  }
}
