import { Component, Input } from '@angular/core';
import { ModalController, NavController } from 'ionic-angular';

@Component({
  selector: 'card-offers',
  templateUrl: 'card-offers.html'
})
export class CardOffersComponent {

  @Input('horizontal') horizontal: boolean = false;

  product: {
    id: number,
    price: number,
    price_discount_price_two: number,
    price_discount: number,
    cumulative: number,
    price_cumulative_per_product: number,
    cumulative_payable: number,
    view_percent_discount: boolean,
    percent_discount_per_product: number,
    top_offer: boolean,
    validity?: Date,
    limit_per_coupon: number,
    unity: string,
    brand: string,
    description: string,
    images: Array<Object>,
  }

  button_add: boolean = true;

  constructor(public modalCtrl: ModalController, public navCtrl: NavController) {
    
    this.product = {
      id: null,
      price: null,
      price_discount_price_two: null,
      price_discount: null,
      cumulative: null,
      price_cumulative_per_product: null,
      cumulative_payable: null,
      view_percent_discount: false,
      percent_discount_per_product: null,
      top_offer: false,
      limit_per_coupon: null,
      unity: null,
      brand: null,
      description: null,
      images: [],
    };
  }

  goPage() {
    this.navCtrl.push('ViewProductPage', {id: this.product.id})
  }

  @Input()
  set button_add_list(data: boolean) {
    this.button_add = data;
  }

  @Input()
  set id(data: number) {
    this.product.id = data;
  }

  @Input()
  set price(data: number) {
    this.product.price = data;
  }

  @Input()
  set priceDiscount(data: number) {
    this.product.price_discount = data;
  }

  @Input()
  set priceDiscountPriceTwo(data: number) {
    this.product.price_discount_price_two = data;
  }

  @Input()
  set cumulative(data: number) {
    this.product.cumulative = data;
  }

  @Input()
  set priceCumulativePerProduct(data: number) {
    this.product.price_cumulative_per_product = data;
  }

  @Input()
  set cumulativePayable(data: number) {
    this.product.cumulative_payable = data;
  }

  @Input()
  set viewPercentDiscount(data: boolean) {
    this.product.view_percent_discount = data;
  }

  @Input()
  set percentDiscountPerProduct(data: number) {
    this.product.percent_discount_per_product = data;
  }

  @Input()
  set topOffer(data: boolean) {
    this.product.top_offer = data;
  }

  @Input()
  set limitPerCoupon(data: number) {
    this.product.limit_per_coupon = data;
  }

  @Input()
  set description(data: string) {
    this.product.description = data;
  }

  @Input()
  set brand(data: string) {
    this.product.brand = data;
  }

  @Input()
  set unity(data: string) {
    this.product.unity = data;
  }

  @Input()
  set validity(data: Date) {
    this.product.validity = data;
  }

  @Input()
  set images(data: Array<Object>) {
    if (data) {
      data.forEach((element, index) => {
        data[index]['url'] = `http://54.233.242.37/image/product/${element['filename']}?format=jpg&quality=80&width=100`
      });

      this.product.images = data;
    }
  }

  openModalAddProduct(product) {
    const modal = this.modalCtrl.create(
      'ModalAddProductListPage',
      { product: product },
      { enableBackdropDismiss: true, showBackdrop: true, cssClass: 'inset-modal' }
    )
    modal.present();

    modal.onDidDismiss(data => {
      
    });
  }
}
