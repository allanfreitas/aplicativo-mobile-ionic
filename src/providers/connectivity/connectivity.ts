import { Injectable } from '@angular/core';
import { Network } from '@ionic-native/network';

@Injectable()
export class ConnectivityProvider {


  constructor(public network: Network){
  }

  public connectionType(): string {
    return this.network.type;
  }

  public isOnline(): boolean {
    return this.network.type != 'none' && this.network.type != 'unknown';
  }

  public isOffline(): boolean {
    return this.network.type == 'none' || this.network.type == 'unknown';
  }

  watchOnline(): any {
    return this.network.onConnect();
  }

  watchOffline(): any {
    return this.network.onDisconnect();
  }

}
