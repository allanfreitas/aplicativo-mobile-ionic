import { Injectable } from '@angular/core';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/retry';

import { ApiService } from '../api';

@Injectable()
export class OffersProvider {

  constructor(public api: ApiService) {
  }

  public getOffersProductsProgress(column: string = '', text: string = '') {
    return new Promise((resolve, reject) => {
      this.api.get('app/offers/progress/products')
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }
}
